//
//  TableViewSettingsCell.swift
//  IphoneSettings
//
//  Created by Busazhida on 2/2/21.
//

import Foundation
import UIKit
import SnapKit

class TableViewSettingsCell: BaseTVCell {
    
    private lazy var imgView: UIImageView = {
        let view = UIImageView()
        view.layer.cornerRadius = 8
        return view
    }()
    
    private lazy var title: UILabel = {
        let view = UILabel()
        view.textAlignment = .left
        return view
    }()
    
    override func addSubViews() {
        addSubview(imgView)

        addSubview(title)
    }
    override func setupUI() {
        imgView.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(10)
            make.top.bottom.equalToSuperview().offset(10).inset(10)
            make.width.equalTo(30)
            make.height.equalTo(30)
        }
        
        title.snp.makeConstraints { (make) in
            make.left.equalTo(self.imgView.snp.right).offset(15)
            make.centerY.equalToSuperview()
            make.width.equalToSuperview().multipliedBy(0.8)
            make.height.equalToSuperview().offset(4).inset(4)
        }
    }
    func setTitle(_ data: String) {
        title.text = data
    }
    func setImage(_ data: String) {
        imgView.backgroundColor = .random()
    }
}
