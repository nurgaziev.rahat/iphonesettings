//
//  TableViewProfileCell.swift
//  IphoneSettings
//
//  Created by Busazhida on 2/6/21.
//

import Foundation
import UIKit
import SnapKit

class TableViewProfileCell: BaseTVCell {
    
    lazy var imgView: UIImageView = {
        let view = UIImageView()
        view.layer.cornerRadius = 24
        return view
    }()
    
    lazy var titleLabel: UILabel = {
        let view = UILabel()
        view.font = UIFont.boldSystemFont(ofSize: CGFloat(16))
        view.textColor = .blue
        return view
    }()
    lazy var descriptionLabel: UILabel = {
        let view = UILabel()
        view.font = UIFont.boldSystemFont(ofSize: CGFloat(12))
        view.textColor = .lightGray
        return view
    }()
    override func addSubViews() {
        addSubview(imgView)
        addSubview(titleLabel)
        addSubview(descriptionLabel)
    }
    
    override func setupUI() {
        imgView.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(10)
            make.bottom.equalToSuperview().inset(10)
            make.leading.equalToSuperview().offset(10)
            make.width.equalTo(55)
            make.height.equalTo(55)
        }
        titleLabel.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(15)
            make.left.equalTo(imgView.snp.right).offset(10)
            make.width.equalToSuperview().multipliedBy(0.7)
            make.height.equalToSuperview().multipliedBy(0.4)
        }
        descriptionLabel.snp.makeConstraints { (make) in
            make.top.equalTo(titleLabel.snp.bottom)
            make.left.equalTo(imgView.snp.right).offset(10)
            make.width.equalToSuperview().multipliedBy(0.7)
            make.height.equalTo(20)
        }
    }
    func setImage(_ data: String) {
        imgView.backgroundColor = .random()
    }
    
    func setLablesText(_ title: String,_ description: String){
        titleLabel.text = title
        descriptionLabel.text = description
    }
}
