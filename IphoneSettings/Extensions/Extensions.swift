//
//  Extensions.swift
//  IphoneSettings
//
//  Created by Busazhida on 2/6/21.
//

import Foundation
import SnapKit

extension UITableViewCell {
    static var cellID: String {
        return className
    }
    
    static var nibName: String {
        return className
    }
}

extension NSObject {
    var className: String {
        return type(of: self).className
    }
    
    static var className: String {
        return String(describing: self)
    }
}
