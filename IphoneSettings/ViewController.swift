//
//  ViewController.swift
//  IphoneSettings
//
//  Created by Busazhida on 2/2/21.
//

import UIKit
import SnapKit

class ViewController: UIViewController {
   
    
    lazy var mySections: [[SettingsModel]] = {
        
        let section0 = [
            SettingsModel(type: 2,title:"Sign in to your Iphone",description: "Setup Icloud, the AppStore and more ", image: "profile"),
        ]
        let section1 = [
            SettingsModel(type: 1,title:"General",image: "general"),
            SettingsModel(type: 1,title: "Accessibily",image: "accessibility"),
            SettingsModel(type: 1,title: "Privacy",image: "privacy")
        ]
        let section2 = [
            SettingsModel(type: 1,title:"Passwords",image: "passwords"),
        ]
        let section3 = [
            SettingsModel(type: 1,title:"Safari",image: "safari"),
            SettingsModel(type: 1,title:"News",image: "news"),
            SettingsModel(type: 1,title:"Maps",image: "maps"),
            SettingsModel(type: 1,title:"ShortCuts",image: "shortcuts"),
            SettingsModel(type: 1,title:"Health",image: "health"),
            SettingsModel(type: 1,title:"Siri & Search",image: "siri"),
            SettingsModel(type: 1,title:"Photos",image: "photos"),
            SettingsModel(type: 1,title:"Game Center",image: "gameCenter"),
        ]
        
        let section4 = [
            SettingsModel(type: 1,title:"Developer",image: "developer"),
        ]

        return [section0,section1, section2, section3, section4]
    }()
    lazy var tableView: UITableView = {
        let view = UITableView(frame: .zero,style: UITableView.Style.grouped)
        view.separatorStyle = .singleLine
        view.dataSource = self
        view.delegate = self
        view.isScrollEnabled = true
        view.register(TableViewSettingsCell.self, forCellReuseIdentifier: TableViewSettingsCell.cellID)
        view.register(TableViewProfileCell.self, forCellReuseIdentifier: TableViewProfileCell.cellID)
        view.backgroundColor = .clear
        view.rowHeight = UITableView.automaticDimension
        view.estimatedRowHeight = 44
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        self.navigationItem.title = "Settings"
        self.navigationController?.navigationBar.prefersLargeTitles = true
        view.backgroundColor = UIColor.init(hex: "#F5F5F5")
        view.addSubview(tableView)
        tableView.snp.makeConstraints { (make) in
            make.top.equalToSuperview()
            make.left.equalToSuperview().offset(5)
            make.height.equalToSuperview()
            make.width.equalToSuperview()
        }
    }
    
}
extension ViewController : UITableViewDelegate{
   
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("section: \(indexPath.section)")
        print("row: \(indexPath.row)")
        
        self.tableView.reloadData()
    }
}

extension ViewController: UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let data = self.mySections[indexPath.section][indexPath.row]
        
        if data.type == 1 {
            let cell = TableViewSettingsCell()
            cell.setTitle(data.title)
            cell.setImage(data.image)
            return cell
        } else if data.type == 2 {
            let cell = TableViewProfileCell()
            cell.setImage(data.image)
            cell.setLablesText(data.title, data.description!)
            return cell
        } else {
            return UITableViewCell()
        }
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.mySections[section].count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.mySections.count
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.mySections[section].count
    }
}
