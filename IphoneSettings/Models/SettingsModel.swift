//
//  SettingsModel.swift
//  IphoneSettings
//
//  Created by Busazhida on 2/2/21.
//

import Foundation

struct SettingsModel {
    var type: Int
    var title: String
    var description: String?
    var image: String
}
