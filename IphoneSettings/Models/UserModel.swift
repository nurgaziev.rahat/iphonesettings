//
//  UserModel.swift
//  IphoneSettings
//
//  Created by Busazhida on 2/2/21.
//

import Foundation

struct UserModel {
    var type: Int
    var image: String
    var title: String
    var description: String
}
